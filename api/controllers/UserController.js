/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  get:function(req, res){
    User.find().populate('roles').exec(function(err, users){
      if(err){res.json(err);}
      res.json(users);
    });
  },
  getRoles:function(req, res){
    var username = req.params.username;
    User.find({username:username}).populate('roles').exec(function(err, users){
      if(err){res.json(err)}
      res.json(users);
    });
  }
};

