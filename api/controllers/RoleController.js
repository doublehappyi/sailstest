/**
 * RoleController
 *
 * @description :: Server-side logic for managing roles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	get:function(req, res){
    Role.find().populate('users').exec(function(err, roles){
      if(err){res.json(err)}
      res.json(roles);
    });
  },
  getUsers:function(req, res){
    var name = req.params.name;
    Role.findOne({name:name}).populate('users').exec(function(err, roles){
      if(err){res.json(err)}
      res.json(roles);
    });
  }
};

