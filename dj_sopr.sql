create database if not exists dj_sopr;
use dj_sopr;

create table if not exists user(
  `id` INT(10) unsigned NOT NULL PRIMARY KEY,
  `username` VARCHAR(20) UNIQUE NOT NULL,
  `password` VARCHAR(20) DEFAULT '88888888'
);

INSERT INTO user values (1, "admin", "admin"), (2, "yishuangxi", "yishuangxi");

create table if not exists role(
  `id` INT(10) unsigned NOT NULL PRIMARY KEY,
  `name` VARCHAR(20) UNIQUE NOT NULL
);
INSERT INTO role values (1, "admin"), (2, "guest");

create table if not exists user_role(
  `userId` INT(10) NOT NULL,
  `roleId` INT(10) NOT NULL,
  PRIMARY KEY (userId, roleId)
);
INSERT INTO user_role values (1, 1),(1, 2),(2, 2);
